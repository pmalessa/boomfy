import threading
import time
import logging

import RPi.GPIO as GPIO

logger = logging.getLogger("boomfy")

#Button Input Pins
Button_Pin = 16

class buttonThread (threading.Thread):
    def __init__(self, pin):
        threading.Thread.__init__(self)
        self.shutdown_flag = threading.Event()
        self.pressed = 0
        self.pin = pin
        GPIO.setmode(GPIO.BCM) 
        GPIO.setup(self.pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) # Input with pull-down

    def run(self):
        logger.debug('buttonThread started')
        while not self.shutdown_flag.is_set():
            if GPIO.event_detected(self.pin):
                logger.debug('Button press detected')
                self.pressed = 1
            time.sleep(0.2)
        logger.debug('buttonThread stopped')
        GPIO.cleanup(self.pin)
        
buttonT = buttonThread(Button_Pin)    # Create buttonThread

def init():
    logger.debug('Button Init')
    buttonT.start()

def getPress():
    if buttonT.pressed == 1:
        buttonT.pressed = 0
        return 1
    else:
        return 0
    
def stop():
    buttonT.shutdown_flag.set()    # Stop buttonThread
    buttonT.join()
    