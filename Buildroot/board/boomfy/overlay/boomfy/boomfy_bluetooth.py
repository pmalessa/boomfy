import threading
import time
import logging
import dbus
import bluez_utils
import boomfy_led_shift as LED

logger = logging.getLogger("boomfy")     

class btThread (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.shutdown_flag = threading.Event()
        self.error = 0
        self.bus = dbus.SystemBus()
        self.adapter_obj = bluez_utils.find_adapter()
        self.adapter_if = dbus.Interface(self.adapter_obj, bluez_utils.ADAPTER_INTERFACE)
        self.adapter_prop_if = dbus.Interface(self.adapter_obj, bluez_utils.DBUS_PROP_IFACE)
        self.desired_power = 0
        self.desired_discov = 0
        self.connected = 0

    def run(self):
        logger.debug('BT Adapter: '+self.adapter_prop_if.Get(bluez_utils.ADAPTER_INTERFACE, 'Address'))
        logger.debug('btThread started')
        while not self.shutdown_flag.is_set():
            self.check_states()#check if state is desired state repeatedly
            self.set_led()
            time.sleep(1.5)
        logger.debug('btThread stopped')
    def get_connected_devices(self):
        con_dev = []
        devices_obj = bluez_utils.get_all_devices()
        for dev_obj in devices_obj:
            dev_prop_if = dbus.Interface(dev_obj, bluez_utils.DBUS_PROP_IFACE)
            if int(dev_prop_if.Get(bluez_utils.DEVICE_INTERFACE, 'Connected')) == 1:
                   con_dev.append(dev_obj)
        return con_dev
    def check_states(self):
        power = int(self.adapter_prop_if.Get(bluez_utils.ADAPTER_INTERFACE, 'Powered'))#get power state
        if power != self.desired_power:
            logger.debug("Power "+str(power)+" -> "+str(self.desired_power))
            self.adapter_prop_if.Set(bluez_utils.ADAPTER_INTERFACE, 'Powered', dbus.Boolean(self.desired_power))
        discov = int(self.adapter_prop_if.Get(bluez_utils.ADAPTER_INTERFACE, 'Discoverable'))#get power state
        if discov != self.desired_discov:
            logger.debug("Discoverable "+str(discov)+" -> "+str(self.desired_discov))
            self.adapter_prop_if.Set(bluez_utils.ADAPTER_INTERFACE, 'Discoverable', dbus.Boolean(self.desired_discov))
        if len(self.get_connected_devices()) > 0:
            if self.connected == 0: #if state changed
                logger.debug("Connected 0 -> 1")
                self.connected = 1
        else:
            if self.connected == 1: #if state changed
                logger.debug("Connected 1 -> 0")
                self.connected = 0
    def disconnect_devices(self):
        for con_dev in self.get_connected_devices():
            dev_if = dbus.Interface(con_dev, bluez_utils.DEVICE_INTERFACE)
            dev_prop_if = dbus.Interface(con_dev, bluez_utils.DBUS_PROP_IFACE)
            logger.debug('Disconnect '+ str(dev_prop_if.Get(bluez_utils.DEVICE_INTERFACE, 'Address')))
            dev_if.Disconnect()
    def set_led(self):
        if self.connected == 1:
            LED.On(LED.LED_BLUE)
        elif self.desired_discov == 1:
            LED.Blink(LED.LED_BLUE)
        else:
            LED.Off(LED.LED_BLUE)
        
btT = btThread()    # Create buttonThread

def init():
    logger.debug('BT Init')
    btT.start()
    
def setPower(state):
    btT.desired_power = state
    
def setDiscoverable(state):
    btT.desired_discov = state

def setDisconnect():
    btT.disconnect_devices()
    
def isDiscoverable():
    btT.get_discoverable()
    
def isConnected():
    if len(btT.get_connected_devices()) > 0:
        return 1
    else:
        return 0
    
def getError():
    return btT.error
    
def stop():
    btT.shutdown_flag.set()    # Stop buttonThread
    btT.join()
