import time
import signal 
import logging

TIMEOUT_SEC = 15 #define Timeout value

import boomfy_led_shift as LED
import boomfy_button as Button
import boomfy_wlan as WLAN
import boomfy_bluetooth as Bluetooth

state = "STARTUP"
button = 0
quit = 0
timeout = 0
cnt = 0

#stop function
def stop(signum, frame):
    logger.info('Exiting BoomFy')
    LED.stop()
    Button.stop()
    WLAN.stop()
    Bluetooth.stop()
    Button.stop()
    exit()

#init logger
formatstr = '%(asctime)s,%(msecs)03d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s'
logging.basicConfig(format = formatstr, datefmt='%d-%m-%Y:%H:%M:%S', level=logging.DEBUG)
logger = logging.getLogger("boomfy")
handler = logging.FileHandler('/var/log/boomfy.log')
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(formatstr)
handler.setFormatter(formatter)
logger.addHandler(handler)

#Signal Init
signal.signal(signal.SIGTERM, stop)# Register the signal handlers
signal.signal(signal.SIGINT, stop)

#START
logger.info('Starting BoomFy')

#Init Modules
LED.init()
Button.init()
WLAN.init()
Bluetooth.init()

#loop
while quit==0:
    if Button.getPress() == 1:
        logger.debug("Button Press detected")
        button = 1
        
    if state == "STARTUP":
        logger.info("Startup state")
        LED.On(LED.LED_RED)
        LED.On(LED.LED_GREEN)
        time.sleep(1)
        state = "IDLE"
        Bluetooth.setDiscoverable(1)
        Bluetooth.setPower(1)
        LED.Off(LED.LED_RED)
        LED.Off(LED.LED_GREEN)
        
    if state == "IDLE":
        #####decide if server or client init#########
        if button == 1: #if button pressed, try to connect to server (init client)
            button = 0
            state = "INIT_CLIENT"
            LED.Blink(LED.LED_GREEN)
            LED.Off(LED.LED_RED)
            logger.info("Init Client state")
            WLAN.setNextState(WLAN.STATE_CLIENT)
            Bluetooth.setDisconnect()
            Bluetooth.setDiscoverable(0)
            Bluetooth.setPower(0)
            timeout = 0
        elif Bluetooth.isConnected() == 1:    #if Bluetooth is connected, act as Server
            state = "INIT_SERVER"
            LED.Blink(LED.LED_RED)
            LED.Off(LED.LED_GREEN)
            logger.info("Init Server state")
            WLAN.setNextState(WLAN.STATE_SERVER)
            timeout = 0
        #############################################
        cnt = cnt + 1
        time.sleep(0.5)
        if cnt > 5: #flash led shortly
            cnt = 0
            LED.On(LED.LED_RED)
            LED.On(LED.LED_GREEN)
            time.sleep(0.2)
            LED.Off(LED.LED_RED)
            LED.Off(LED.LED_GREEN)
        
    elif state == "INIT_SERVER":
        if WLAN.getCurrentState() != WLAN.STATE_SERVER:
            time.sleep(0.5)#wait
            timeout+= 0.5
            if timeout > TIMEOUT_SEC or WLAN.getError() != 0:
                logger.info("Timeout or Error at Server Init")
                WLAN.Reset()    #Reset WLAN and try again
                LED.Blink(LED.LED_RED)
                LED.Blink(LED.LED_GREEN)
                time.sleep(3)
                LED.Off(LED.LED_GREEN)
                timeout = 0
        else:
            state = "SERVER"
            LED.Off(LED.LED_GREEN)
            LED.On(LED.LED_RED)
            logger.info("Server state")
        if button==1:
            button = 0
            logger.info("Switch to Client Mode")
            state = "INIT_CLIENT"
            WLAN.setNextState(WLAN.STATE_CLIENT)
            Bluetooth.setDisconnect()
            Bluetooth.setDiscoverable(0)
            Bluetooth.setPower(0)
            timeout=0
            LED.Blink(LED.LED_GREEN)
            LED.Off(LED.LED_RED)
            
    elif state == "SERVER":
        if Bluetooth.isConnected() != 1:
            logger.info("Bluetooth Connection Lost, return to IDLE")
            WLAN.setNextState(WLAN.STATE_OFF)
            state = "STARTUP"
            continue
        #stay here till button press
        if button==1:
            button = 0
            logger.info("Switch to Client Mode")
            state = "INIT_CLIENT"
            WLAN.setNextState(WLAN.STATE_CLIENT)
            Bluetooth.setDisconnect()
            Bluetooth.setDiscoverable(0)
            Bluetooth.setPower(0)
            timeout=0
            LED.Blink(LED.LED_GREEN)
            LED.Off(LED.LED_RED)
        else:
            nop=0   #inside Server state
            time.sleep(1)
            
    elif state == "INIT_CLIENT":
        if WLAN.getCurrentState() != WLAN.STATE_CLIENT:
            time.sleep(0.5)#wait
            timeout+= 0.5
            if timeout > TIMEOUT_SEC or WLAN.getError() != 0:
                logger.info("Timeout or Error at Client Init")
                WLAN.Reset()    #Reset WLAN and try again
                LED.Blink(LED.LED_RED)
                LED.Blink(LED.LED_GREEN)
                time.sleep(3)
                LED.Off(LED.LED_RED)
                LED.Off(LED.LED_GREEN)
                state = "IDLE"  #back to IDLE
                timeout = 0
        else:
            state = "CLIENT"
            LED.On(LED.LED_GREEN)
            LED.Off(LED.LED_RED)
            logger.info("Client state")
        if button==1:
            button = 0
            logger.info("Switch to Server Mode")
            state = "INIT_SERVER"
            WLAN.setNextState(WLAN.STATE_SERVER)
            Bluetooth.setDiscoverable(1)
            Bluetooth.setPower(1)
            LED.Blink(LED.LED_RED)
            LED.Off(LED.LED_GREEN)
            
    elif state == "CLIENT":
        #stay here till button press
        if button==1:
            button = 0
            logger.info("Switch to Server Mode")
            state = "INIT_SERVER"
            WLAN.setNextState(WLAN.STATE_SERVER)
            Bluetooth.setDiscoverable(1)
            Bluetooth.setPower(1)
            LED.Blink(LED.LED_RED)
            LED.Off(LED.LED_GREEN)
        else:
            nop=0   #inside Client state
            time.sleep(1)
    else:
        a=6 #Dunno
        time.sleep(1)
