import threading
import time
import logging
import subprocess
import os

STATE_OFF = 0
STATE_SERVER_INIT = 1
STATE_SERVER = 2
STATE_CLIENT_INIT = 3
STATE_CLIENT = 4
STATE_INTERNET = 5
STATE_STRING = ["OFF","Server Init","Server","Client Init","Client","Internet"]

SERVER_IP = "170.0.0.1"
CLIENT_IP_PREFIX = "170.0.0."
AVAHI_IP_PREFIX = "169.254."

logger = logging.getLogger("boomfy")

HOSTAPD_PID = "/var/run/hostapd.pid"
DNSMASQ_PID = "/var/run/dnsmasq.pid"
WPA_SUPPLICANT_PID = "/var/run/wpa_supplicant/pid"
UDHCPC_PID = "/var/run/udhcpc.wlan0.pid"

#need control over:
#SERVER: hostapd, dnsmasq, wpa_supplicant
#CLIENT: udhcpd, wpa_supplicant

class wlanThread (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.shutdown_flag = threading.Event()
        self.currentstate = STATE_OFF
        self.nextstate = STATE_OFF
        self.error = 0

    def run(self):
        logger.debug('wlanThread started')
        while not self.shutdown_flag.is_set():
            if self.currentstate == STATE_OFF:
                if self.isUp() != STATE_OFF:  #if wlan0 up
                    self.ifdown()
                if self.nextstate == STATE_SERVER:  #change to SERVER
                    self.ifup_server()
                    self.switch(STATE_SERVER_INIT)
                elif self.nextstate == STATE_CLIENT:  #change to CLIENT
                    self.ifup_client()
                    self.switch(STATE_CLIENT_INIT)
                elif self.nextstate == STATE_INTERNET:  #change to INTERNET
                    self.ifup_internet()
                    self.switch(STATE_INTERNET)
            elif self.currentstate == STATE_SERVER_INIT:
                if self.get_ip() == STATE_SERVER:  #wait till ip is static
                    self.switch(STATE_SERVER)   #then switch to state server
                if self.nextstate != STATE_SERVER:  #switch
                    self.switch(STATE_OFF)
            elif self.currentstate == STATE_SERVER:
                if self.get_ip() != STATE_SERVER:  #check if ip not static anymore
                    logger.debug("ip not static anymore")
                    self.switch(STATE_OFF)
                    continue
                if self.IsRunning(HOSTAPD_PID) != 1 or self.IsRunning(DNSMASQ_PID) != 1: #check if hostapd and dnsmasq still running
                    logger.debug("hostapd or dnsmasq not running anymore")
                    self.switch(STATE_OFF)
                    continue
                if self.nextstate != STATE_SERVER:  #switch
                    self.switch(STATE_OFF)
            elif self.currentstate == STATE_CLIENT_INIT:
                if self.get_ip() == STATE_CLIENT:  #wait until wlan has an ip
                    self.switch(STATE_CLIENT)
                    continue
                if self.IsRunning(WPA_SUPPLICANT_PID) != 1 or self.IsRunning(UDHCPD_PID) != 1:
                    logger.debug("wpa or udhcp not running")
                    self.switch(STATE_OFF)
                if self.nextstate != STATE_CLIENT:  #switch
                    self.switch(STATE_OFF)
            elif self.currentstate == STATE_CLIENT:
                if self.serverUp() != 1:  #ping server to check active connection
                    self.switch(STATE_OFF)
                if self.nextstate != STATE_CLIENT:  #switch
                    self.switch(STATE_OFF)
            elif self.currentstate == STATE_INTERNET:
                self.internetUp()
                if self.nextstate != STATE_INTERNET:  #switch
                    self.switch(STATE_OFF)
            time.sleep(2)
        logger.debug('wlanThread stopped')

    def serverUp(self):
        response = os.system("ping -c 1 " + SERVER_IP)
        if response == 0:
            logger.debug("ping server: up")
            return 1
        else:
            logger.debug("ping server: down")
            return 0

    def internetUp(self):
        response = os.system("ping -c 1 " + "8.8.8.8")
        if response == 0:
            logger.debug("ping internet: up")
            return 1
        else:
            logger.debug("ping internet: down")
            return 0

    def isUp(self): #check state of wlan0 interface
        logger.debug("IsUp: ")
        if os.path.isfile("/run/network/ifstate.wlan0"):
            with open("/run/network/ifstate.wlan0") as f:
                content = f.read()
                if "server" in content:
                    logger.debug("server")
                    return STATE_SERVER
                elif "client" in content:
                    logger.debug("client")
                    return STATE_CLIENT
                elif "internet" in content:
                    logger.debug("internet")
                    return STATE_INTERNET
                else:
                    logger.debug("empty")
                    return STATE_OFF
        else:
            logger.debug("off")
            return STATE_OFF
        
    def get_ip(self):
        try:
            ipv4 = str(os.popen('ip addr show wlan0').read().split("inet ")[1].split("/")[0])
            logger.debug("get_ip: "+ipv4)
            if ipv4 == SERVER_IP:  #170.0.0.1
                return STATE_SERVER
            elif CLIENT_IP_PREFIX in ipv4:  #170.0.0.XXX
                return STATE_CLIENT
            elif AVAHI_IP_PREFIX in ipv4:  #169.254.XXX.XXX -> avahi ip, no real ip
                return STATE_OFF
            else:
                return STATE_OFF
        except:
            logger.debug("get_ip: NO_IP")
            return STATE_OFF
            
        return str(ipv4)
    def ifdown(self):
        logger.debug("ifdown")
        subprocess.call(["ifdown", "wlan0"]) #call ifdown wlan0

    def ifup_server(self):
        logger.debug("ifup server")
        subprocess.call(["ifup", "wlan0=server"]) #call ifup server
        
    def ifup_client(self):
        logger.debug("ifup client")
        subprocess.call(["ifup", "wlan0=client"]) #call ifup client

    def reset(self):
        logger.debug("RESET WLAN0")
        self.currentstate = STATE_OFF

    def switch(self, state):
        logger.debug("switch: "+STATE_STRING[self.currentstate]+"->"+STATE_STRING[self.nextstate])        
        self.currentstate = state

    def IsRunning(self, pidfile):
        logger.debug("IsRunning: "+pidfile)
        if os.path.isfile(pidfile):
            with open(pidfile) as f:
                pid = int(f.read())
                try:
                    os.kill(pid,0)  #checks if process alive
                    logger.debug("alive")
                    return 1
                except:
                    logger.debug("dead")
                    return 0
        else:
            logger.debug("pidfile missing")
            return 0
        
wlanT = wlanThread()    # Create buttonThread

def init():
    logger.debug('WLAN Init')
    wlanT.start()

def getCurrentState():
    return wlanT.currentstate
    
def setNextState(state):
    if state == STATE_OFF:  #only valid states
        wlanT.nextstate = STATE_OFF
    elif state == STATE_CLIENT or state == STATE_CLIENT_INIT:
        wlanT.nextstate = STATE_CLIENT
    elif state == STATE_SERVER or state == STATE_SERVER_INIT:
        wlanT.nextstate = STATE_SERVER
    elif state == STATE_INTERNET:
        wlanT.nextstate = STATE_INTERNET

def Reset():
    wlanT.reset()
    
def getError():
    return wlanT.error
    
def stop():
    wlanT.shutdown_flag.set()    # Stop buttonThread
    wlanT.join()
