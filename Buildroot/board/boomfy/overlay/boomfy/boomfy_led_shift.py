import logging
import threading
import time

import PiShiftPy as shift
from boomfy_utils import Struct 


logger = logging.getLogger("boomfy")

#LED Constants
LED_PWR = 7
LED_BOOTED = 6  
LED_ONLINE = 5
LED_VALVE = 4
LED_S1 = 3
LED_S2 = 2
LED_S3 = 1
LED_ERROR = 0

LED_RED = LED_VALVE
LED_GREEN = LED_ONLINE
LED_BLUE = LED_S3

#LED Config
Led_SER_Pin = 6
Led_RCLK_Pin = 5
Led_SRCLK_Pin = 12

class ledThread (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.shutdown_flag = threading.Event()
        
    def run(self):
        logger.debug('ledThread started')
        while not self.shutdown_flag.is_set():
            for i in range(8):
                shift.push_bit(self.checkLed(i))
            shift.write_latch()
            time.sleep(0.1)
        logger.debug('ledThread stopped')

    def checkLed(self, id):
        if Leds[id].delay == -1: #always off
            return 0
        elif Leds[id].delay == 0: #always on
            return 1
        elif Leds[id].cnt > Leds[id].delay:
            Leds[id].cnt = 0
            if Leds[id].toggle == 1:
                Leds[id].toggle = 0
                return 0
            else:
                Leds[id].toggle = 1
                return 1
        else:
            Leds[id].cnt+= 100
            return Leds[id].toggle


Leds = [Struct(cnt=0, delay=-1, toggle=0) for i in range(8)]  #init Led array
ledT = ledThread()    # Create ledThread

def init():#LED Init
    shift.init(Led_SER_Pin, Led_SRCLK_Pin, Led_RCLK_Pin, 1)  # Initialize with DataPin = GPIO17, Clock=GPIO27, Latch=GPIO22 with 2 chained registers
    setLed(LED_PWR,2000)
    ledT.start()    # Start ledThread
    
def setLed(name, delay):  #setLed: name is a defined name, delay is the wait time between toggle. -1 for off, 0 for always on
    if name >= 0 and name < 8:
        Leds[name].delay = delay
        Leds[name].cnt = 0
  
def On(led):
    setLed(led, 0)
    
def Blink(led):
    setLed(led, 200)
    
def Off(led):
    setLed(led, -1)
    
def stop():
    ledT.shutdown_flag.set()    # Stop ledThread
    ledT.join()
