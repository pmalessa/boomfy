import threading
import time
import logging

import RPi.GPIO as GPIO

logger = logging.getLogger("boomfy")

LED_RED = 0
LED_GREEN = 1
LED_BLUE = 2

#LED Pins
LED_Red_Pin = 17
LED_Green_Pin = 18
LED_Blue_Pin = 19

class ledThread (threading.Thread):
    def __init__(self, pin):
        threading.Thread.__init__(self)
        self.shutdown_flag = threading.Event()
        self.pin = pin
        self.mode = 0
        GPIO.setmode(GPIO.BCM) 
        GPIO.setup(self.pin, GPIO.OUT) # Output

    def run(self):
        logger.debug('ledThread started')
        while not self.shutdown_flag.is_set():
            if self.mode == 0:  #off
                GPIO.output(self.pin, GPIO.LOW)
            elif self.mode == 1: #on
                GPIO.output(self.pin, GPIO.HIGH)
            elif self.mode == 2:   #blink, switch mode between 2 and 3
                GPIO.output(self.pin, GPIO.HIGH)
                self.mode = 3
            else:
                GPIO.output(self.pin, GPIO.LOW)
                self.mode = 2
            time.sleep(0.2)
        logger.debug('ledThread stopped')
        GPIO.cleanup(self.pin)
        
    def setmode(self, mode):
        self.mode = mode
LedRedT = ledThread(LED_Red_Pin)        # Create red LED Thread
LedGreenT = ledThread(LED_Green_Pin)    # Create green LED Thread
LedBlueT = ledThread(LED_Blue_Pin)    # Create blue LED Thread

def init():
    logger.debug('LED Init')
    LedRedT.start()
    LedGreenT.start()
    LedBlueT.start()
    

def On(led):
    if led == 0:
        LedRedT.setmode(1)
    elif led == 1:
        LedGreenT.setmode(1)
    else:
        LedBlueT.setmode(1)
def Blink(led):
    if led == 0:
        LedRedT.setmode(2)
    elif led == 1:
        LedGreenT.setmode(2)
    else:
        LedBlueT.setmode(2)
    
def Off(led):
    if led == 0:
        LedRedT.setmode(0)
    elif led == 1:
        LedGreenT.setmode(0)
    else:
        LedBlueT.setmode(0)
    
def stop():
    LedRedT.shutdown_flag.set()    # Stop LedRedThread
    LedGreenT.shutdown_flag.set()    # Stop LedGreenThread
    LedRedT.join()
    LedGreenT.join()
    LedBlueT.join()
