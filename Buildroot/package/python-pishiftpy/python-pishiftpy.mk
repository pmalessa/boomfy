################################################################################
#
# python-pishiftpy
#
################################################################################

PYTHON_PISHIFTPY_VERSION = 0.1.1
PYTHON_PISHIFTPY_SOURCE = PiShiftPy-$(PYTHON_PISHIFTPY_VERSION).tar.gz
PYTHON_PISHIFTPY_SITE = https://files.pythonhosted.org/packages/7f/e3/14dd97b207bbddd8fd8f91eb1cfa94a3117f1ae4e92cc13aef9d8e98e329
PYTHON_PISHIFTPY_SETUP_TYPE = setuptools
PYTHON_PISHIFTPY_LICENSE = MIT
PYTHON_PISHIFTPY_LICENSE_FILES = LICENSE

$(eval $(python-package))
