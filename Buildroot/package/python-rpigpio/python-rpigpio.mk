################################################################################
#
# python-rpigpio
#
################################################################################

PYTHON_RPIGPIO_VERSION = 0.6.3
PYTHON_RPIGPIO_SOURCE = RPi.GPIO-$(PYTHON_RPIGPIO_VERSION).tar.gz
PYTHON_RPIGPIO_SITE = https://files.pythonhosted.org/packages/e2/58/6e1b775606da6439fa3fd1550e7f714ac62aa75e162eed29dbec684ecb3e
PYTHON_RPIGPIO_SETUP_TYPE = distutils

$(eval $(python-package))
