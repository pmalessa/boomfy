################################################################################
#
# bluez-tools
#
################################################################################

BLUEZ_TOOLS_VERSION = master
BLUEZ_TOOLS_SITE = https://github.com/khvzak/bluez-tools
BLUEZ_TOOLS_SITE_METHOD = git
BLUEZ_TOOLS_DEPENDENCIES = libglib2
BLUEZ_TOOLS_LICENSE = GPLv3
BLUEZ_TOOLS_LICENSE_FILES = COPYING
BLUEZ_TOOLS_INSTALL_TARGET = YES
BLUEZ_TOOLS_AUTORECONF = YES
BLUEZ_TOOLS_AUTORECONF_OPTS = --force --install --verbose

$(eval $(autotools-package))
