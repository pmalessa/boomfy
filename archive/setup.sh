#!/bin/bash

# With help from:
# https://bbs.nextthing.co/t/basic-guide-to-turning-chip-into-a-bluetooth-audio-receiver-audio-sink/2187
# https://possiblelossofprecision.net/?p=1956
# http://kodi.wiki/view/HOW-TO:Autostart_Kodi_for_Linux#Add_a_new_systemd_script

NAME=${NAME:-CHIP Bluetooth speaker}
DEFAULT_PIN=${DEFAULT_PIN:-0000}

# Exit on failure
set -e
# Debug
# set -x

# Update, install the necessary plugins

apt-get update
apt-get upgrade -y
apt-get install -y bluez-tools pulseaudio-module-bluetooth pulseaudio hostapd dnsmasq

# See git log for details
cp -f bt-agent.bin /usr/bin/bt-agent
chmod +x /usr/bin/bt-agent

# Setup default hostname (and Bluetooth adapter name)
#hostnamectl set-hostname "$NAME"
# FIXME: When the systemd version supports it
# hostnamectl set-chassis embedded
#HOST_NAME=`hostname`
#sed -i "s|chip$|$HOST_NAME|" /etc/hosts

# Setup bluez
cat <<EOF > /etc/bluetooth/audio.conf
[General]
Disable=Socket
Enable=Media,Source,Sink,Gateway
EOF

mkdir -p /home/chip/.config/
cat <<EOF > /home/chip/.config/bluetooth-default-pin
* $DEFAULT_PIN
EOF
chown -R chip:chip /home/chip/.config/
chmod 0600 /home/chip/.config/bluetooth-default-pin

cat <<EOF > /etc/systemd/system/bt-agent.service
[Unit]
Description=Bluetooth pairing agent

[Install]
WantedBy=multi-user.target

[Service]
User=chip
Group=chip
Type=simple
PrivateTmp=true
ExecStart=/usr/bin/bt-agent -c NoInputNoOutput -p /home/chip/.config/bluetooth-default-pin
Restart=on-failure
TimeoutStopSec=5
EOF

# http://bluetooth-pentest.narod.ru/software/bluetooth_class_of_device-service_generator.html
# Major Service Class: Rendering/Audio
# Major Device Class: Audio/Video
# Minor Device Class: Loudspeaker/HiFi Audio Device
sed -i 's|^Class =|Class=0x24043C|; s|^#Class =|Class=0x24043C|' /etc/bluetooth/main.conf

systemctl restart bluetooth

bt-adapter --set Powered 1
bt-adapter --set DiscoverableTimeout 0
bt-adapter --set Discoverable 1
bt-adapter --set PairableTimeout 0
bt-adapter --set Pairable 1

systemctl enable bt-agent.service
systemctl restart bt-agent.service

# Setup pulseaudio
cat <<EOF > /etc/systemd/system/pulseaudio.service
[Unit]
Description=PulseAudio Daemon
[Install]
WantedBy=multi-user.target
[Service]
User=root
Group=root
Type=simple
PrivateTmp=true
ExecStart=/usr/bin/pulseaudio -v --system --realtime=false --high-priority=false
Restart=on-abort
RestartSec=5
EOF

cat <<EOF > /etc/dbus-1/system.d/pulseaudio-bluetooth.conf
<!-- This configuration file specifies the required security policies
     for PulseAudio Bluetooth integration. -->

<!DOCTYPE busconfig PUBLIC "-//freedesktop//DTD D-BUS Bus Configuration 1.0//EN"
 "http://www.freedesktop.org/standards/dbus/1.0/busconfig.dtd">
<busconfig>

  <!-- ../system.conf have denied everything, so we just punch some holes -->

  <policy user="pulse">
    <allow own="org.bluez"/>
    <allow send_destination="org.bluez"/>
    <allow send_interface="org.bluez.Agent1"/>
    <allow send_interface="org.bluez.MediaEndpoint1"/>
    <allow send_interface="org.bluez.MediaPlayer1"/>
    <allow send_interface="org.bluez.ThermometerWatcher1"/>
    <allow send_interface="org.bluez.AlertAgent1"/>
    <allow send_interface="org.bluez.Profile1"/>
    <allow send_interface="org.bluez.HeartRateWatcher1"/>
    <allow send_interface="org.bluez.CyclingSpeedWatcher1"/>
    <allow send_interface="org.bluez.GattCharacteristic1"/>
    <allow send_interface="org.bluez.GattDescriptor1"/>
    <allow send_interface="org.freedesktop.DBus.ObjectManager"/>
    <allow send_interface="org.freedesktop.DBus.Properties"/>
  </policy>

  <policy at_console="true">
    <allow send_destination="org.bluez"/>
  </policy>

  <policy context="default">
    <deny send_destination="org.bluez"/>
  </policy>

</busconfig>
EOF

cat <<EOF > /etc/network/interfaces
# interfaces(5) file used by ifup(8) and ifdown(8)
# Include files from /etc/network/interfaces.d:
source-directory /etc/network/interfaces.d

auto wlan0

# server configuration

iface server inet static
        address 170.0.0.1
        netmask 255.255.255.0
 	up /etc/init.d/dnsmasq restart
        post-up /etc/init.d/hostapd restart
        pre-down /etc/init.d/hostapd stop
        down /etc/init.d/dnsmasq stop

#client configuration

iface client inet dhcp
        #wpa-driver wext
	wpa-ssid "BoomFy"
	wpa-psk "boomboom"

iface internet inet dhcp
	wpa-ssid "Malessa"
	wpa-psk "InselWLAN9"
EOF

cat <<EOF > /etc/dnsmasq.conf
# Configuration file for dnsmasq.
dhcp-range=170.0.0.10,170.0.0.50,12h

EOF

cp -f hostapd /etc/init.d/hostapd
chmod +x /etc/init.d/hostapd

cat <<EOF > /etc/hostapd/hostapd.conf
interface=wlan0
driver=nl80211
ssid=BoomFy
country_code=DE
hw_mode=g
channel=1
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=boomboom
wpa_key_mgmt=WPA-PSK
wpa_pairwise=CCMP
wpa_group_rekey=86400
ieee80211n=1
wme_enabled=1
EOF

cat <<EOF > /etc/pulse/system.pa
#!/usr/bin/pulseaudio -nF

load-module module-native-protocol-unix auth-anonymous=1

load-module module-always-sink
load-module module-switch-on-connect

### Automatically load driver modules for Bluetooth hardware
.ifexists module-bluetooth-policy.so
load-module module-bluetooth-policy
.endif

.ifexists module-bluetooth-discover.so
load-module module-bluetooth-discover
.endif

load-module module-pipe-sink file=/home/chip/snapfifo sink_name=Snapcast
update-sink-proplist Snapcast device.description=Snapcast

set-default-sink Snapcast

EOF

cat <<EOF > /etc/pulse/client.conf
autospawn = no
EOF

systemctl enable pulseaudio.service
systemctl restart pulseaudio.service

cp -f ui_handler.py /home/chip/ui_handler.py
chmod +x ui_handler.py

wget https://github.com/badaix/snapcast/releases/download/v0.11.1/snapserver_0.11.1_armhf.deb
dpkg -i snapserver_0.11.1_armhf.deb

wget https://github.com/badaix/snapcast/releases/download/v0.11.1/snapclient_0.11.1_armhf.deb
dpkg -i snapclient_0.11.1_armhf.deb

cat <<EOF > /etc/default/snapclient
START_SNAPCLIENT=true
USER_OPTS="--user snapclient:audio"
SNAPCLIENT_OPTS="-d -h 170.0.0.1"
EOF

cat <<EOF > /etc/default/snapserver
START_SNAPSERVER=true
USER_OPTS="--user snapserver:snapserver"

#SNAPSERVER_OPTS="-d --sampleformat 44100:16:2"
SNAPSERVER_OPTS="-d -s pipe:///home/chip/snapfifo?name=Snapcast&mode=read --sampleformat 44100:16:2"
EOF


cat <<EOF > /lib/systemd/system/snapclient.service
[Unit]
Description=Snapcast client
After=network-online.target sound.target
Requires=network-online.target

[Service]
EnvironmentFile=-/etc/default/snapclient
Type=forking
ExecStart=/usr/bin/snapclient -d $USER_OPTS $SNAPCLIENT_OPTS
PIDFile=/var/run/snapclient/pid
Restart=always
TimeoutStopSec=5

[Install]
WantedBy=multi-user.target
EOF

cat <<EOF > /etc/rc.local
#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.

sudo python3 /home/chip/ui_handler.py &

exit 0
EOF



